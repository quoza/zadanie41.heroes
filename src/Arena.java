import java.util.LinkedList;
import java.util.List;

public class Arena {
    private List<Hero> theGoodGuys;
    private List<Hero> theBadGuys;

    public Arena() {
        this.theGoodGuys = new LinkedList<>();
        this.theBadGuys = new LinkedList<>();
    }

    public void addHero(Hero hero){
        if(hero.getSide().equals(Side.EVIL)){
            theBadGuys.add(hero);
        } else {
            theGoodGuys.add(hero);
        }
    }

    private void makeFirstHeroBoss(Hero hero){
        hero.findFirstHero(this).setIsBoss(true);
    }

    public List<Hero> getTheGoodGuys() {
        return theGoodGuys;
    }

    public List<Hero> getTheBadGuys() {
        return theBadGuys;
    }

    public void setTheGoodGuys(List<Hero> theGoodGuys) {
        this.theGoodGuys = theGoodGuys;
    }

    public void setTheBadGuys(List<Hero> theBadGuys) {
        this.theBadGuys = theBadGuys;
    }
}

//  5. Kolejnym etapem będzie stworzenie areny. Będzie to klasa (Arena) instancjonowana w
//        metodzie main(). Klasa powinna mieć dwa prywatne pola, które będą listami bohaterów:
//        -List<Hero> goodGuys
//        -List<Hero> badGuys
//        Listy mają być inicjalizowane w konstruktorze jako puste listy.
//        Klasa Arena powinna mieć metodę addHero(Hero Hero), w której dodaje się
//        bohatera do odpowiedniej listy, zważywszy na Side konkretnego bohatera.
//        Metoda sama powinna sprawdzić, do jakiej strony należy dany bohater.
//        Najstarszy bohater zostaje bossem (weź pod uwagę pole spawnDate) i jeszcze
//        raz dostaje bonus (taki jak w konstruktorze).