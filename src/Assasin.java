public class Assasin extends Hero {

    public Assasin(String name, Side side) {
        super(name, side);
        setAttackPoints(getAttackPoints() * 2.5);
    }

    @Override
    public void doSpecialMove() {

    }
}
