public class Priest extends Hero {

    public Priest(String name, Side side) {
        super(name, side);
        setHealthPoints(getHealthPoints() * 1.5);
    }

    @Override
    public void doSpecialMove() {

    }
}
