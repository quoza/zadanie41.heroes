public class Tank extends Hero{

    public Tank(String name, Side side) {
        super(name, side);
        setHealthPoints(getHealthPoints() * 2 );
    }

    @Override
    public void doSpecialMove() {

    }
}
