import java.time.LocalDateTime;
import java.util.ArrayList;

public abstract class Hero {
    private String name;
    private double healthPoints;
    private double attackPoints;
    private double defencePoints;
    private Side side;
    private LocalDateTime spawnDate;
    private boolean isBoss;

    public Hero(String name, Side side) {
        this.name = name;
        this.side = side;
        this.healthPoints = 1000;
        boolean isBoss =  false;
        this.attackPoints = 100;
        this.defencePoints = 50;
        this.spawnDate = LocalDateTime.now();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsBoss(){ return isBoss; }

    public void setIsBoss(boolean boss) {
        isBoss = boss;
    }

    public double getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(double healthPoints) {
        this.healthPoints = healthPoints;
    }

    public double getAttackPoints() {
        return attackPoints;
    }

    public void setAttackPoints(double attackPoints) {
        this.attackPoints = attackPoints;
    }

    public double getDefencePoints() {
        return defencePoints;
    }

    public void setDefencePoints(double defencePoints) {
        this.defencePoints = defencePoints;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public LocalDateTime getSpawnDate() {
        return spawnDate;
    }

    public void setSpawnDate(LocalDateTime spawnDate) {
        this.spawnDate = spawnDate;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "name='" + name + '\'' +
                ", healthPoints=" + healthPoints +
                ", attackPoints=" + attackPoints +
                ", defencePoints=" + defencePoints +
                ", side=" + side +
                ", spawnDate=" + spawnDate +
                '}';
    }

    public abstract void doSpecialMove();

    public void attack(Hero enemy){
        enemy.getAttacked(getAttackPoints());
    }

    public void getAttacked(double attackPoints){
        if(attackPoints <= getDefencePoints()){
            System.out.println("0 damage.");
        }
        else{
            setHealthPoints(getHealthPoints() - (attackPoints - getDefencePoints()));
        }
    }

    public Hero findFirstHero(Arena arena){
        ArrayList<Hero> allHeroes = new ArrayList<>();
        allHeroes.addAll(arena.getTheGoodGuys());
        allHeroes.addAll(arena.getTheBadGuys());
        Hero firstHero = allHeroes.get(0);

        for (Hero heroInList : allHeroes){
            if (firstHero.getSpawnDate().isAfter(heroInList.getSpawnDate())){
                heroInList = firstHero;
            }
        }

        return firstHero;
    }
}
